import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ShowclientsComponent } from './showclients/showclients.component';
import { ShowaccountsComponent } from './showaccounts/showaccounts.component';
import { TransferComponent } from './transfer/transfer.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ShowclientsComponent,
    ShowaccountsComponent,
    TransferComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'showClients', component: ShowclientsComponent },
      { path: 'showAccounts', component: ShowaccountsComponent },
      { path: 'transfer', component: TransferComponent },
      { path: 'welcome', component: WelcomeComponent },
      { path: '', redirectTo: '/welcome', pathMatch: 'full' },
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
