import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TransferService } from '../service/transfer.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  idSource: number = 1;
  idDest: number = 2;
  amount: number = 10;
  @ViewChild('transferForm') transferForm: NgForm;

  constructor(private transferService: TransferService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.log("transfer")
    this.transferService.transfer(this.idSource, this.idDest, this.amount)
  }

}
