# 1.0.0 (2023-03-24)


### Features

* Initial Load ([158188a](https://gitlab.com/rtgr-demos/bankfront/commit/158188a6ae90454f09d431bf8c377f6628db4924))

## [1.0.12](https://gitlab.com/cafat2/bankfront/compare/v1.0.11...v1.0.12) (2023-03-23)


### Bug Fixes

* ok ([1eaa84d](https://gitlab.com/cafat2/bankfront/commit/1eaa84dcc0324b11d684bceca1fe13b8a988eab5))

## [1.0.11](https://gitlab.com/cafat2/bankfront/compare/v1.0.10...v1.0.11) (2023-03-23)


### Bug Fixes

* ok ([5a0b984](https://gitlab.com/cafat2/bankfront/commit/5a0b984b44f9ab0308143ff8dea206a3aeece282))

## [1.0.10](https://gitlab.com/cafat2/bankfront/compare/v1.0.9...v1.0.10) (2023-03-23)


### Bug Fixes

* test ([4567e80](https://gitlab.com/cafat2/bankfront/commit/4567e8063bad5c74ce3e212e192beba327bb6663))

## [1.0.9](https://gitlab.com/cafat2/bankfront/compare/v1.0.8...v1.0.9) (2023-03-23)


### Bug Fixes

* ok ([1d0d389](https://gitlab.com/cafat2/bankfront/commit/1d0d3895bb8765d99813391bb17fbb988b0cb573))

## [1.0.8](https://gitlab.com/cafat2/bankfront/compare/v1.0.7...v1.0.8) (2023-03-23)


### Bug Fixes

* ok ([ce2e4c4](https://gitlab.com/cafat2/bankfront/commit/ce2e4c47fc7ae28a0b87611cbf50fb85ceca7ba5))

## [1.0.7](https://gitlab.com/cafat2/bankfront/compare/v1.0.6...v1.0.7) (2023-03-23)


### Bug Fixes

* dotenv ([5269c47](https://gitlab.com/cafat2/bankfront/commit/5269c4720f42a728f5b28aa5be3d6515eb37616d))

## [1.0.6](https://gitlab.com/cafat2/bankfront/compare/v1.0.5...v1.0.6) (2023-03-23)


### Bug Fixes

* env var ([d2573b1](https://gitlab.com/cafat2/bankfront/commit/d2573b1ada4aba5ac1fb3ba3b3829d686aa19e8d))

## [1.0.5](https://gitlab.com/cafat2/bankfront/compare/v1.0.4...v1.0.5) (2023-03-23)


### Bug Fixes

* pass version as parameter ([288bd5c](https://gitlab.com/cafat2/bankfront/commit/288bd5c51b5ecf2fb87cd12183a7fd5a802e0573))

## [1.0.4](https://gitlab.com/cafat2/bankfront/compare/v1.0.3...v1.0.4) (2023-03-23)


### Bug Fixes

* test env var ([29ba693](https://gitlab.com/cafat2/bankfront/commit/29ba6937bdd11e6dc42baed4602fad762115435c))

## [1.0.3](https://gitlab.com/cafat2/bankfront/compare/v1.0.2...v1.0.3) (2023-03-23)


### Bug Fixes

* test ([8c2ad81](https://gitlab.com/cafat2/bankfront/commit/8c2ad81aeea28b63ec292e770a4271aeb4b73194))

## [1.0.2](https://gitlab.com/cafat2/bankfront/compare/v1.0.1...v1.0.2) (2023-03-23)


### Bug Fixes

* docker image version ([1de2473](https://gitlab.com/cafat2/bankfront/commit/1de2473307655660710cf57d4eee78f7770d9d3a))

## [1.0.1](https://gitlab.com/cafat2/bankfront/compare/v1.0.0...v1.0.1) (2023-03-23)


### Bug Fixes

* refactoring ([30573ce](https://gitlab.com/cafat2/bankfront/commit/30573ceed1f21f890fb6966099a2aa1363783287))

# 1.0.0 (2023-03-23)


### Bug Fixes

* build docker iamge ([a4edb19](https://gitlab.com/cafat2/bankfront/commit/a4edb197dfe88d92a74bba68db3ab852684672ad))
* change stage ([5b7d6f2](https://gitlab.com/cafat2/bankfront/commit/5b7d6f2783f25272960ef426ae2cc5c461903eb1))
* folder name ([8e7b46a](https://gitlab.com/cafat2/bankfront/commit/8e7b46adc244c84fe352057ea0415278b04e26a5))
* image construction ([bd01977](https://gitlab.com/cafat2/bankfront/commit/bd01977dac4d00c079195265d7e8957339f006c4))


### Features

* Creation ([446e2fd](https://gitlab.com/cafat2/bankfront/commit/446e2fd193656c0055025c168771269a6fce399f))
* ws call ([7cc48b7](https://gitlab.com/cafat2/bankfront/commit/7cc48b7e6d0eb1b9d92961bace69068fff215d3f))
