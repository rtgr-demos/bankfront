import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransferService {

  constructor(private httpClient: HttpClient) {
  }

  transfer(idSource: number, idDest: number, amount: number) {
    let url = '/api/transfer/' + idSource + '/' + idDest + '/' + amount
    console.log("url : " + url)
    this.httpClient.get(url);
  }
}