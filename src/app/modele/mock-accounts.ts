import { Account } from "./Account"

export const ACCOUNTS: Account[] = [
    { id: "0", numero: "0", description: "desc1", solde: 100 },
    { id: "1", numero: "1", description: "desc2", solde: 300 },
    { id: "2", numero: "2", description: "desc3", solde: 300 },
]