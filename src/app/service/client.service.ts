import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from '../modele/Client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {


  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Client[]> {
    return this.httpClient.get<Client[]>('/api/client');
  }
}
