import { Component, OnInit } from '@angular/core';
import { Client } from '../modele/Client';
import { CLIENTS } from '../modele/mock-clients';
import { ClientService } from '../service/client.service';

@Component({
  selector: 'app-showclients',
  templateUrl: './showclients.component.html',
  styleUrls: ['./showclients.component.css']
})
export class ShowclientsComponent implements OnInit {

  clients: Client[];


  constructor(private clientService: ClientService) {
  }

  ngOnInit(): void {
    this.refreshData();
  }

  private refreshData(): void {
    this.clientService.getAll().subscribe((data) => {
      this.clients = data;
    });
  }

}
