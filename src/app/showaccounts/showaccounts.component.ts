import { Component, OnInit } from '@angular/core';
import { Account } from '../modele/Account';
import { ACCOUNTS } from '../modele/mock-accounts';
import { AccountService } from '../service/account.service';

@Component({
  selector: 'app-showaccounts',
  templateUrl: './showaccounts.component.html',
  styleUrls: ['./showaccounts.component.css']
})
export class ShowaccountsComponent implements OnInit {
  accounts: Account[];

  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
    this.refreshData();
  }

  private refreshData(): void {
    this.accountService.getAll().subscribe((data) => {
      this.accounts = data;
    });
  }

}
