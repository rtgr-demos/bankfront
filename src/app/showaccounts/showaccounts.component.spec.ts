import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowaccountsComponent } from './showaccounts.component';

describe('ShowaccountsComponent', () => {
  let component: ShowaccountsComponent;
  let fixture: ComponentFixture<ShowaccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowaccountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowaccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
